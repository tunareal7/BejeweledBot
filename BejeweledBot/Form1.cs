﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BejeweledBot
{
    public partial class Form1 : Form
    {
        private Form overlay;
        private Board board;
        private Timer refreshTimer = new Timer();
        private Rectangle overlayArea;
        private VirtualMouse virtualMouse = new VirtualMouse();
        Font drawFont = new Font("Arial", 8);

        public Form1()
        {
            KeyboardIntercept keyboardIntercept = new KeyboardIntercept();
            keyboardIntercept.KeyIntercepted += new EventHandler<KeyEventArgs>(Form_KeyDown);
            InitializeComponent();
            pibDebug.CreateGraphics();
            overlay = new OverlayForm();
            refreshTimer.Interval = 600;
            refreshTimer.Tick += refreshTimer_Tick;
        }

        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            using (Bitmap bmp = ScreenCap.Grab(overlayArea))
            {
                board.Update(bmp);
                drawColorRep(board.TileColors);
            }
            Move bestMove = MoveHandler.GetBestMove(board.SimplifiedTiles);
            if(bestMove != null)
            {
                Point click = new Point(overlay.Location.X + bestMove.Pt.X * 40 + 20, overlay.Location.Y + bestMove.Pt.Y * 40 + 20);
                virtualMouse.Move(click.X, click.Y);
                virtualMouse.Click();
                if (bestMove.Direction == Direction.UP)
                    click.X -= 40;
                else if (bestMove.Direction == Direction.DOWN)
                    click.X += 40;
                else if (bestMove.Direction == Direction.LEFT)
                    click.Y -= 40;
                else if (bestMove.Direction == Direction.RIGHT)
                    click.Y += 40;
                virtualMouse.Move(click.X, click.Y);
                virtualMouse.Click();
            }
        }

        private void btnGrabScreen_Click(object sender, EventArgs e)
        {
            overlay.Hide();
            overlayArea = new Rectangle(overlay.Location.X,
                                        overlay.Location.Y,
                                        overlay.Width,
                                        overlay.Height);
            using (Bitmap bmp = ScreenCap.Grab(overlayArea))
            {
                board = new Board(40, bmp);
            }
            refreshTimer.Start();
        }

        private void drawColorRep(Color[,] colors) //TODO: refactor
        {
            int side = 40;
            int width = colors.GetLength(0);
            int height = colors.GetLength(1);
            for (int row = 0; row < width; row++)
                for(int col = 0; col < height; col++)
                {
                    using (SolidBrush br = new SolidBrush(colors[row, col]))
                    using (Graphics g = pibDebug.CreateGraphics())
                    {
                        g .FillRectangle(br, new Rectangle(row * side, col * side, side, side));
                        string colorString = board.SimplifiedTiles[row, col].ToString();

                        g.DrawString(colorString, drawFont, new SolidBrush(Color.Black), row * side, col * side);
                    }
                }
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            refreshTimer.Stop();
            overlay.Show();
        }

        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                refreshTimer.Stop();
            }
        }
    }
}
