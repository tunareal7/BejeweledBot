﻿using System.Drawing;
using System.Windows.Forms;

namespace BejeweledBot
{
    class OverlayForm : Form
    {
        private int X { get; set; }
        private int Y { get; set; }
        private bool isDragged = false;
        private Point moveStartPoint;

        public OverlayForm(int width = 330, int height = 330)
        {
            BackColor = Color.Blue;
            FormBorderStyle = FormBorderStyle.None;
            Bounds = new Rectangle(0, 0, width, height);
            MouseDown += overlay_MouseDown;
            MouseUp += overlay_MouseUp;
            MouseMove += overlay_MouseMove;
            Opacity = 0.2;
            TopMost = true;
            Show();
        }

        private void overlay_MouseDown(object sender, MouseEventArgs e)
        {
            moveStartPoint = new Point(e.X, e.Y);
            isDragged = true;
        }

        private void overlay_MouseUp(object sender, MouseEventArgs e)
        {
            isDragged = false;
        }

        private void overlay_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragged)
            {
                Point p1 = new Point(e.X, e.Y);
                Point p2 = PointToScreen(p1);
                Point p3 = 
                Location = new Point(p2.X - moveStartPoint.X,
                                     p2.Y - moveStartPoint.Y);
            }
        }
    }
}
